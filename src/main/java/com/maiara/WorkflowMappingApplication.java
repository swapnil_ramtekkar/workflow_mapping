package com.maiara;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkflowMappingApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkflowMappingApplication.class, args);
	}

}
