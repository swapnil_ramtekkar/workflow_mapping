package com.maiara.workflow.utils;

import java.util.UUID;

/**
 * @author Microtek
 *
 *         Feb 22, 2018
 */
public class GenerateUUID {

  private GenerateUUID() {

  }

  public static String getUuid() {
    UUID uuid = UUID.randomUUID();
    return uuid.toString();
  }
}
