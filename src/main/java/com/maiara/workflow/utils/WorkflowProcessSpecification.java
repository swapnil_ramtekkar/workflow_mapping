package com.maiara.workflow.utils;
import java.util.Date;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.maiara.workflow.entity.WorkflowProcess;

/**
 * @author Microtek
 */
 
public class WorkflowProcessSpecification {
 public static Specification<WorkflowProcess> idContains(Long id) {
    return StringUtils.isEmpty(id) ? null : new Specification<WorkflowProcess>() {
      @Override
      public Predicate toPredicate(Root<WorkflowProcess> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        return cb.equal(root.get("id"), id);

      }
    };
  }
  public static Specification<WorkflowProcess> projectNameContains(String projectName) {
    return StringUtils.isEmpty(projectName) ? null : new Specification<WorkflowProcess>() {
      String containsLikePattern = getContainsLikePattern(projectName);

      @Override
      public Predicate toPredicate(Root<WorkflowProcess> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return cb.like(cb.lower(root.get("project_name")), containsLikePattern);
      }
    };
  }
  public static Specification<WorkflowProcess> commentContains(String comment) {
    return StringUtils.isEmpty(comment) ? null : new Specification<WorkflowProcess>() {
      String containsLikePattern = getContainsLikePattern(comment);

      @Override
      public Predicate toPredicate(Root<WorkflowProcess> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        return cb.like(cb.lower(root.get("comment")), containsLikePattern);
      }
    };
  }
 public static Specification<WorkflowProcess> isActiveContains(Boolean isActive) {
    return StringUtils.isEmpty(isActive) ? null : new Specification<WorkflowProcess>() {
      @Override
      public Predicate toPredicate(Root<WorkflowProcess> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        return cb.equal(root.get("is_active"), isActive);

      }
    };
  }

  /**
   * This method is used to implement case-insensitive search
   * 
   * @param searchTerm
   * @return
   */
  private static String getContainsLikePattern(String searchTerm) {
    if (searchTerm == null || searchTerm.isEmpty()) {
      return "%";
    } else {
      return "%" + searchTerm.toLowerCase(Locale.ENGLISH) + "%";
    }
  }

}
