package com.maiara.workflow.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.maiara.workflow.dto.PaginationDTO;
import com.maiara.workflow.dto.WorkflowProcessDTO;
import com.maiara.workflow.entity.WorkflowProcess;
import com.maiara.workflow.error.BusinessException;
import com.maiara.workflow.error.TechnicalException;
import com.maiara.workflow.repository.WorkflowProcessRepository;
import com.maiara.workflow.service.WorkflowProcessService;
import com.maiara.workflow.utils.GenerateUUID;
import com.maiara.workflow.utils.WorkflowProcessSpecification;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author Microtek
 *
 */
 
@Service("workflowProcessService")
public class WorkflowProcessServiceImpl implements WorkflowProcessService {

  private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowProcessServiceImpl.class);

  @Autowired
  @Qualifier("workflowProcessRepository")
  private WorkflowProcessRepository workflowProcessRepository;
  

	/**
	 * Save  WorkflowProcess data.
	 *
	 * @throws TechnicalException
	 */
	@Override
	public WorkflowProcessDTO saveWorkflowProcess(WorkflowProcessDTO workflowProcessDTO)
			throws TechnicalException {
			
		WorkflowProcess workflowProcess = new WorkflowProcess();
			
		try{

			/**** Converting dto to entity. */
			workflowProcess = convertToWorkflowProcessEntity(workflowProcessDTO, workflowProcess);
			
			/** Save & send Response by converting it DTO */
			return convertToWorkflowProcessDTO(workflowProcessRepository.save(workflowProcess));
		
		} catch (Exception e) {
			LOGGER.error("Error while saving WorkflowProcess data ", e);
			throw new TechnicalException("Error while saving WorkflowProcess data");
		}
		
	}
	
	/**
	 * Update  WorkflowProcess data.
	 *
	 * @throws TechnicalException
	 */
	@Override
	public WorkflowProcessDTO updateWorkflowProcess(WorkflowProcessDTO workflowProcessDTO)
			throws TechnicalException {			
		WorkflowProcess workflowProcess = new WorkflowProcess();			
		try{
			/**** Converting dto to entity. */
			workflowProcess = forUpdateConvertToWorkflowProcessEntity(workflowProcessDTO, workflowProcess);			
			/** Save & send Response by converting it DTO */
			return convertToWorkflowProcessDTO(workflowProcessRepository.save(workflowProcess));
		
		} catch (Exception e) {
			LOGGER.error("Error while saving WorkflowProcess data ", e);
			throw new TechnicalException("Error while saving WorkflowProcess data");
		}
		
	}
	
	

	/**
	 * This method is used to search WorkflowProcess into database based on the provided criteria
	 *
	 * @throws TechnicalException
	 */
	@Override
	public PaginationDTO fetchWorkflowProcessDetails(WorkflowProcessDTO workflowProcessDTO) throws TechnicalException {
		WorkflowProcess workflowProcessObj=new WorkflowProcess();
		List<WorkflowProcessDTO> workflowProcessDtoList = null;
		Page<WorkflowProcess> workflowProcessList = null;
		PaginationDTO paginationDTO = new PaginationDTO();
		try {
			/**
			 * this block of code fetch the data based on user given input
			 */
           WorkflowProcess workflowProcess = convertToWorkflowProcessEntity(workflowProcessDTO,workflowProcessObj);
			workflowProcessList = workflowProcessRepository.findAll(
					WorkflowProcessSpecification.idContains(workflowProcess.getId() != null
									? workflowProcess.getId()									
									: workflowProcess.getId())
                          .and(WorkflowProcessSpecification
									.projectNameContains(workflowProcess.getProjectName() != null 
									      ? workflowProcess.getProjectName().trim()
										  : workflowProcess.getProjectName()))
                          .and(WorkflowProcessSpecification
									.commentContains(workflowProcess.getComment() != null 
									      ? workflowProcess.getComment().trim()
										  : workflowProcess.getComment()))
                          .and(WorkflowProcessSpecification
									.isActiveContains(workflowProcess.getIsActive() != null 
										  ? workflowProcess.getIsActive()
										  : workflowProcess.getIsActive()))
					 , PageRequest.of(workflowProcessDTO.getPageNumber(), workflowProcessDTO.getPageSize(), Direction.ASC,
							"id"));
							
			//setting up the pagination elements to display pagination on UI.
			paginationDTO.setTotalRecords(workflowProcessList.getTotalElements());
			paginationDTO.setPageCount(workflowProcessList.getTotalPages());
			workflowProcessDtoList = workflowProcessList.getContent().stream()
					.map(WorkflowProcessMap -> convertToWorkflowProcessDTO(WorkflowProcessMap)).collect(Collectors.toList());
			paginationDTO.setWorkflowProcessList(workflowProcessDtoList);
		} catch (Exception e) {
			LOGGER.error("Error while fetching WorkflowProcess records", e);
			throw new TechnicalException("Error while fetching WorkflowProcess data");
		}
		return paginationDTO;
	}
	


	/*** Convert Entity to DTO */
	@Override
	public WorkflowProcessDTO convertToWorkflowProcessDTO(WorkflowProcess workflowProcess) {
		SimpleDateFormat formatter=new SimpleDateFormat("dd-MM-yyyy");
		WorkflowProcessDTO workflowProcessDTO = new WorkflowProcessDTO();
		workflowProcessDTO.setUuid(workflowProcess.getUuid());
			workflowProcessDTO.setId(workflowProcess.getId());		
			workflowProcessDTO.setProjectName(workflowProcess.getProjectName());		
			workflowProcessDTO.setComment(workflowProcess.getComment());		
			workflowProcessDTO.setIsActive(workflowProcess.getIsActive());		
		
		return workflowProcessDTO;
	}
	
	
	/*** Convert DTO to Entity */
	@Override
	public WorkflowProcess convertToWorkflowProcessEntity(WorkflowProcessDTO workflowProcessDTO, WorkflowProcess workflowProcess) throws TechnicalException, BusinessException{
		SimpleDateFormat formatter=new SimpleDateFormat("dd-MM-yyyy");
		try {
		workflowProcess.setUuid(workflowProcessDTO.getUuid() != null ? workflowProcessDTO.getUuid() : GenerateUUID.getUuid());
			workflowProcess.setId(workflowProcessDTO.getId());		
			workflowProcess.setProjectName(workflowProcessDTO.getProjectName());		
			workflowProcess.setComment(workflowProcessDTO.getComment());		
			workflowProcess.setIsActive(workflowProcessDTO.getIsActive());		
		}
		catch(Exception e){
	        LOGGER.error("Error while converting dto to entity for WorkflowProcess", e);
			throw new TechnicalException("Error while saving WorkflowProcess data");		
		}
		
		return workflowProcess;
	}
	
	/*** Convert DTO to Entity for update*/
	@Override
	public WorkflowProcess forUpdateConvertToWorkflowProcessEntity(WorkflowProcessDTO workflowProcessDTO, WorkflowProcess workflowProcess) throws TechnicalException, BusinessException{
		SimpleDateFormat formatter=new SimpleDateFormat("dd-MM-yyyy");
		try {
		workflowProcess=findByUuid(workflowProcessDTO.getUuid());
			workflowProcess.setId(workflowProcessDTO.getId());		
			workflowProcess.setProjectName(workflowProcessDTO.getProjectName());		
			workflowProcess.setComment(workflowProcessDTO.getComment());		
			workflowProcess.setIsActive(workflowProcessDTO.getIsActive());		
		}
		catch(Exception e){
	        LOGGER.error("Error while converting dto to entity for WorkflowProcess", e);
			throw new TechnicalException("Error while saving WorkflowProcess data");		
		}
		
		return workflowProcess;
	}
	
	
	/*** Find  WorkflowProcess entity by UUID */
	@Override
	public WorkflowProcess findByUuid(String Uuid){
	
	return workflowProcessRepository.findByUuid(Uuid);
	
	}
}
	
