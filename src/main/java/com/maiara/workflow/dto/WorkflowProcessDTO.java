package com.maiara.workflow.dto;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Microtek
 */

@Getter
@Setter
public class WorkflowProcessDTO{

	private String uuid;
  
  private Long id;
  
  @Pattern(regexp = "^[a-zA-Z0-9 ]+[a-zA-Z0-9._ ]*$", message = "Only alphanumeric with (._) are allowed")  
  @Size(max = 100, message = "projectName can not be more than 100 characters.")
  @NotNull(message = "projectName can not be null.")
  private String projectName;
  
  @Size(max = 500, message = "comment can not be more than 500 characters.")
  @NotNull(message = "comment can not be null.")
  private String comment;
  
  @NotNull(message = "isActive can not be null.")
  private Boolean isActive;
 	private Integer pageNumber;
 	private Integer pageSize;
}
