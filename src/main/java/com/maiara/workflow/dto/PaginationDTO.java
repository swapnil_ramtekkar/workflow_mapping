/**
 *
 */
package com.maiara.workflow.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Microtek
 *
 */
@Getter
@Setter
public class PaginationDTO {
	private Long totalRecords;
	private Integer pageCount;	
	
	private List<WorkflowProcessDTO> workflowProcessList;

}
