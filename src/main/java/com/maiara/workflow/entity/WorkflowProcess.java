package com.maiara.workflow.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Microtek
 */
@Entity
@Getter
@Setter

public class WorkflowProcess {

  private String uuid;
  
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
  @SequenceGenerator(name = "pk_sequence", sequenceName = "seq_workflowProcess_id", allocationSize = 1)
  @Column(name = "id" , nullable=false )
  private Long id;
   
  @Column(name = "project_name" , length=100 , nullable=true )
  private String projectName;
   
  @Column(name = "comment" , length=500 , nullable=true )
  private String comment;
   
  @Column(name = "is_active")
  private Boolean isActive;
   
}
