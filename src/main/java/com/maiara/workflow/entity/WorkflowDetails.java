/**
 *
 */
package com.maiara.workflow.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Microtek
 *
 */
@Entity
@Getter
@Setter
@Table(name = "workflow_details")
public class WorkflowDetails {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
  @SequenceGenerator(name = "pk_sequence", sequenceName = "seq_workflow_details_id", allocationSize = 1)
  @Column(name = "id", unique = true, length = 11, nullable = false)
  private Long id;

  @Column(name = "guid", length = 100, nullable = false)
  private String guid;
  
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "workflow_process_id")
  private WorkflowProcess workflowProcessId;

  @Column(name = "workflow_name", length = 100, nullable = false)
  private String workflowName;

  @Column(name = "workflow_details", length = 100, nullable = true)
  private String workflowDetails;

  @Column(name = "max_checker_level", length = 10, nullable = false)
  private Long maxCheckerLevel;

  @Column(name = "is_active")
  private Boolean isActive;

}
