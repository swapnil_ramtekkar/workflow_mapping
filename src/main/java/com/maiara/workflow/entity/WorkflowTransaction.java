/**
 *
 */
package com.maiara.workflow.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Microtek
 *
 */
@Entity
@Getter
@Setter
@Table(name = "workflow_transaction")
public class WorkflowTransaction {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
  @SequenceGenerator(name = "pk_sequence", sequenceName = "seq_workflow_transaction_id", allocationSize = 1)
  @Column(name = "id", unique = true, length = 11, nullable = false)
  private Long id;

  @Column(name = "guid", length = 100, nullable = false)
  private String guid;
  
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "workflow_allocator_id")
  private WorkflowAllocator WorkflowAllocatorId;

  @Column(name = "target_info_uid", length = 100, nullable = false)
  private String targetInfoUid;
  
  @Column(name = "status", length = 100, nullable = false)
  private String status;

  @Column(name = "comment", length = 100, nullable = false)
  private String comment;

  @Column(name = "maker_id", length = 10, nullable = false)
  private Long makerId;

  @Column(name = "checker_id", length = 10, nullable = false)
  private Long checker_id;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "workflow_details_id")
  private WorkflowDetails workflowDetailsId;
  
  @Column(name = "level", length = 10, nullable = false)
  private Long level;


}
