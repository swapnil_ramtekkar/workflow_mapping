/**
 *
 */
package com.maiara.workflow.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Microtek
 *
 */
@Entity
@Getter
@Setter
@Table(name = "workflow_allocator")
public class WorkflowAllocator {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
  @SequenceGenerator(name = "pk_sequence", sequenceName = "seq_workflow_allocator_id", allocationSize = 1)
  @Column(name = "id", unique = true, length = 11, nullable = false)
  private Long id;

  @Column(name = "guid", length = 100, nullable = false)
  private String guid;
  
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "workflow_details_id")
  private WorkflowDetails WorkflowDetailsId;

  @Column(name = "maker", length = 100, nullable = false)
  private String maker;

  @Column(name = "checker", length = 100, nullable = false)
  private String checker;

  @Column(name = "is_next_checker")
  private Boolean isNextChecker;
  
  @Column(name = "level", length = 10, nullable = false)
  private Long level;


}
