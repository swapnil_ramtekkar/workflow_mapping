/**
 *
 */
package com.maiara.workflow.error;

import java.io.PrintStream;
import java.io.PrintWriter;

/**

 */
public class CriticalException extends Exception {

  private Throwable originalException;

  public CriticalException(String messageID) {
    super(messageID);
  }

  public CriticalException(String messageID, Throwable exception) {
    super(messageID);
    this.originalException = exception;
  }

  public CriticalException(Throwable exception) {
    super(exception.getMessage());
    this.originalException = exception;
  }

  @Override
  public void printStackTrace() {
    printStackTrace(System.err);
  }

  @Override
  public void printStackTrace(PrintStream ps) {
    super.printStackTrace(ps);
    if (getOriginalException() != null) {
      getOriginalException().printStackTrace(ps);
    }
  }

  @Override
  public void printStackTrace(PrintWriter pw) {
    super.printStackTrace(pw);
    if (getOriginalException() != null) {
      getOriginalException().printStackTrace(pw);
    }
  }

  protected Throwable getOriginalException() {
    return this.originalException;
  }
}
