/**
 *
 */
package com.maiara.workflow.error;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 *
 */
public class BusinessException extends Exception {
  private Throwable originalException;
  private String exceptionCategoryCode = "";

  public BusinessException() {}

  public BusinessException(String messageID) {
    super(messageID);
  }

  public BusinessException(String messageID, String category) {
    super(messageID);
    this.exceptionCategoryCode = category;
  }

  public BusinessException(String messageID, Throwable exception, String category) {
    super(messageID);
    this.exceptionCategoryCode = category;
    this.originalException = exception;
  }

  public BusinessException(Throwable exception) {
    super(exception.getMessage());
    this.originalException = exception;
  }

  @Override
  public void printStackTrace() {
    printStackTrace(System.err);
  }

  @Override
  public void printStackTrace(PrintStream ps) {
    super.printStackTrace(ps);
    if (getOriginalException() != null) {
      getOriginalException().printStackTrace(ps);
    }
  }

  @Override
  public void printStackTrace(PrintWriter pw) {
    super.printStackTrace(pw);
    if (getOriginalException() != null) {
      getOriginalException().printStackTrace(pw);
    }
  }

  protected Throwable getOriginalException() {
    return this.originalException;
  }

  public String getCategoryCode() {
    return this.exceptionCategoryCode;
  }

  public void setCategoryCode(String messageID) {
    this.exceptionCategoryCode = messageID;
  }

}
