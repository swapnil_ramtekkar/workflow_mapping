package com.maiara.workflow.service;

import java.util.List;

import com.maiara.workflow.dto.WorkflowProcessDTO ;
import com.maiara.workflow.dto.PaginationDTO;
import com.maiara.workflow.entity.WorkflowProcess ;
import com.maiara.workflow.error.BusinessException;
import com.maiara.workflow.error.TechnicalException;

/**
 * @author Microtek
 *
 */
 
public interface WorkflowProcessService {

  public WorkflowProcess convertToWorkflowProcessEntity(WorkflowProcessDTO workflowProcessDTO,WorkflowProcess workflowProcess) throws TechnicalException, BusinessException;

  public WorkflowProcessDTO convertToWorkflowProcessDTO(WorkflowProcess workflowProcess);
  
  WorkflowProcess forUpdateConvertToWorkflowProcessEntity(WorkflowProcessDTO workflowProcessDTO, WorkflowProcess workflowProcess) throws TechnicalException, BusinessException;

  public WorkflowProcessDTO saveWorkflowProcess(WorkflowProcessDTO workflowProcessDTO) throws TechnicalException;

  public WorkflowProcess findByUuid(String Uuid);
  
  public PaginationDTO fetchWorkflowProcessDetails(WorkflowProcessDTO workflowProcessDTO) throws TechnicalException;
  
  public WorkflowProcessDTO updateWorkflowProcess(WorkflowProcessDTO workflowProcessDTO) throws TechnicalException;



}
