package com.maiara.workflow.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maiara.workflow.dto.WorkflowProcessDTO;
import com.maiara.workflow.dto.PaginationDTO;
import com.maiara.workflow.error.BusinessException;
import com.maiara.workflow.error.TechnicalException;
import com.maiara.workflow.service.WorkflowProcessService;

/**
 * @author Microtek
 *
 */

@RestController
@RequestMapping("api/workflowProcess")
public class WorkflowProcessController {

  @Autowired
  @Qualifier("workflowProcessService")
  WorkflowProcessService workflowProcessService;
  
  
  @PostMapping(value = "/saveWorkflowProcess")
  public WorkflowProcessDTO saveWorkflowProcess(@Valid @RequestBody  WorkflowProcessDTO workflowProcessDTO)throws TechnicalException {
    return workflowProcessService.saveWorkflowProcess(workflowProcessDTO);
  }
  
  @PostMapping(value = "/updateWorkflowProcess")
  public WorkflowProcessDTO updateWorkflowProcess(@Valid @RequestBody  WorkflowProcessDTO workflowProcessDTO)throws TechnicalException {  
    return workflowProcessService.updateWorkflowProcess(workflowProcessDTO);
  }


  @PostMapping(value = "/fetchWorkflowProcessDetails")
  public PaginationDTO fetchWorkflowProcess(@RequestBody  WorkflowProcessDTO workflowProcessDTO)throws TechnicalException {
    return workflowProcessService.fetchWorkflowProcessDetails(workflowProcessDTO);
  }

}
