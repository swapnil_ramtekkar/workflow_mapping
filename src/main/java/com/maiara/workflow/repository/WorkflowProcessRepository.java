package com.maiara.workflow.repository;


import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.maiara.workflow.entity.WorkflowProcess;

/**
 * @author Microtek
 */
 
public interface WorkflowProcessRepository extends JpaRepository<WorkflowProcess, Long>, JpaSpecificationExecutor<WorkflowProcess>, PagingAndSortingRepository<WorkflowProcess, Long> {
 
 public WorkflowProcess findByUuid(String uuid);
 
}
